package com.idoideas.boombox;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;


import com.google.android.gms.cast.framework.CastButtonFactory;
import com.idoideas.boombox.Cast.CastManager;
import com.idoideas.boombox.Fragments.AlbumsFragment;
import com.idoideas.boombox.Fragments.ArtistsFragment;
import com.idoideas.boombox.Fragments.PlaylistsFragment;
import com.idoideas.boombox.Fragments.SearchFragment;
import com.idoideas.boombox.Fragments.SongsFragment;
import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.Objects.SongsMachine;
import com.idoideas.boombox.Objects.JukeBox;
import com.idoideas.boombox.WebServer.WebServerHandler;

import static android.content.Context.MODE_PRIVATE;

public class MainView extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    static Context context;
    static Activity This;
    static FragmentManager fragmentManager;
    public static ArrayList<Song> lastList;
    private static Song lastSong;
    static SharedPreferences mPrefs;
    static View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lets_play);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setContext(getApplicationContext());
        checkPermissions();
        fragmentManager = getSupportFragmentManager();
        This = this;
        mPrefs = getPreferences(MODE_PRIVATE);
        view = findViewById(R.id.drawer_layout);

        //Do it once, not every time
        //the user changes the orientation or something.
        if(savedInstanceState == null) {
            //Creating the server
            //if needed
            if(!WebServerHandler.isCreated()){
                WebServerHandler.createWebServer(5555);
            }

            //Setting the view
            replaceFragment(new SongsFragment(SongsMachine.getSongs()));

            //initialize the Jukebox
            JukeBox.init();

            //play the intent song,
            //if one was initiated.
            Song intentSong = getIntentSong();
            if(intentSong!=null){
                JukeBox.play(intentSong);
            }

            //initialize the cast manager
            CastManager.init();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebServerHandler.startWebServer();
                Snackbar.make(view, "Server started at: " + WebServerHandler.getAddress()
                        , Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        //set details to the visual player,
        //everytime the user changes orientation
        if(lastSong!=null){
            setSongDetails(lastSong);
        }
    }

    //Checks for permissions in the way
    //Android Marshmallow+ want to.
    //@TargetApi(Build.VERSION_CODES.M)
    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        ArrayList<String> Permissions = new ArrayList<>();
        //List all permissions here
        Permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        Permissions.add(Manifest.permission.INTERNET);
        Permissions.add(Manifest.permission.ACCESS_WIFI_STATE);
        Permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        Permissions.add(Manifest.permission.WAKE_LOCK);
        Permissions.add(Manifest.permission.MEDIA_CONTENT_CONTROL);
        //End list of permissions

        for(String permission : Permissions) {

                if (checkSelfPermission(permission)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{permission},
                            0);

            }
        }
        }
    }

    //Get the context.
    //Because other classes need that too.
    //Don't take this all to yourself.
    public static Context getContext(){
        return context;
    }

    public static void setContext(Context ct){
        context = ct;
    }

    public static Activity getThis(){
        return This;
    }

    public static View getView(){return view;}

    public static SharedPreferences getPrefrences(){
        return mPrefs;
    }

    //Replaces the main screen fragment.
    public static void replaceFragment(Fragment fragment){
        if (fragment!=null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.LetsPlayContent, fragment);
            ft.commit();
        }
    }

    public static void replaceFragmentWithTransition(Fragment fragment, boolean toLeft){
        if (fragment!=null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if(toLeft){
                ft.setCustomAnimations(R.anim.left_in, R.anim.left_out);
            }
            else{
                ft.setCustomAnimations(R.anim.right_in, R.anim.right_out);
            }
            ft.replace(R.id.LetsPlayContent, fragment);
            ft.commit();
        }
    }

    public static void setSongDetails(Song song){
        setNavDetails(song.getTitle(), song.getArtist(), song.getAlbumArt());
        lastSong = song;
        ImageView playerMainImage = (ImageView) getThis().findViewById(R.id.MainPlayerImage);
        playerMainImage.setImageBitmap(song.getAlbumArt());
        //TODO: REMOVE THAT WHEN I HAVE PROPER CONTROLLERS
        playerMainImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(JukeBox.isPlaying()){
                    JukeBox.pause();
                }
                else{
                    JukeBox.resume();
                }
            }
        });
        TextView playerMainArtist = (TextView) getThis().findViewById(R.id.MainPlayerArtist);
        playerMainArtist.setText(song.getArtist());
        TextView playerMainTitle = (TextView) getThis().findViewById(R.id.MainPlayerTitle);
        playerMainTitle.setText(song.getTitle());
        TextView playerMainAlbum = (TextView) getThis().findViewById(R.id.MainPlayerAlbum);
        playerMainAlbum.setText(song.getAlbumTitle());
    }
    public static void setNavDetails(String name, String artist, Bitmap image){
        TextView nameView = (TextView) getThis().findViewById(R.id.NavName);
        TextView artistView = (TextView) getThis().findViewById(R.id.NavArtist);
        ImageView imageView = (ImageView) getThis().findViewById(R.id.NavImage);

        if(nameView!=null){
            nameView.setText(name);
            artistView.setText(artist);
            imageView.setImageBitmap(getCroppedBitmap(image));

        }
    }

    public static Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    @Override
    protected void onDestroy() {
        WebServerHandler.stopWebServer();
        JukeBox.abandonAudioFocus();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.lets_play, menu);

        MenuItem mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu, R.id.media_route_menu_item);
        menu.findItem(R.id.queue_button).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                replaceFragment(new SongsFragment(JukeBox.getQueueSongs()));
                return false;
            }
        });

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                SearchFragment searchFragment = new SearchFragment();
                SearchFragment.setQuery(newText);
                replaceFragment(searchFragment);
                return false;
            }


        });

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        if (id == R.id.nav_camera) {
            // Handle the camera action
            fragment = new SongsFragment(SongsMachine.getSongs());
        } else if (id == R.id.nav_gallery) {
            fragment = new AlbumsFragment();
        } else if (id == R.id.nav_slideshow) {
            fragment = new ArtistsFragment();
        } else if (id == R.id.nav_manage) {
            fragment = new PlaylistsFragment();
        } else if (id == R.id.nav_share) {
            JukeBox.seekTo(1,0);
        } else if (id == R.id.nav_send) {
            JukeBox.seekTo(50000);
        }

        replaceFragment(fragment);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public Song getIntentSong(){
        Intent intent = getIntent();
        if(intent.getData() != null && intent.getData().toString().contains("file://")){
            try {
                String path = URLDecoder.decode(intent.getData().toString().replace("file://", ""), "UTF-8");
                return SongsMachine.searchSongsPath(path).get(0);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        else if(intent.getAction()!=null) {
            if (intent.getAction().compareTo(MediaStore.INTENT_ACTION_MEDIA_PLAY_FROM_SEARCH) == 0) {
                String mediaFocus, query, title;
                if(intent.getExtras()!=null){
                    mediaFocus = intent.getStringExtra(MediaStore.EXTRA_MEDIA_FOCUS);
                    query = intent.getStringExtra(SearchManager.QUERY);
                    title = intent.getStringExtra(MediaStore.EXTRA_MEDIA_TITLE);
                }
                else{
                    Bundle extras = intent.getBundleExtra("Extras");
                    mediaFocus = extras.getString(MediaStore.EXTRA_MEDIA_FOCUS);
                    query = extras.getString(SearchManager.QUERY);
                    title = extras.getString(MediaStore.EXTRA_MEDIA_TITLE);
                }
                if (mediaFocus == null) {
                    return SongsMachine.searchSongsTitle(query).get(0);
                } else if (mediaFocus.compareTo("vnd.android.cursor.item/*") == 0) {
                    if (query==null||query.isEmpty()) {
                        //"any"
                        return JukeBox.getQueueSong(0);
                    } else {
                        // 'Unstructured' search mode
                        return SongsMachine.searchSongsTitle(query).get(0);
                    }

                } else if (mediaFocus.compareTo("vnd.android.cursor.item/audio") == 0) {
                    // 'Song' search mode
                    return SongsMachine.searchSongsTitle(title).get(0);

                }
            }
        }
        return null;
    }

}
