package com.idoideas.boombox.Fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.idoideas.boombox.Adapters.AlbumsAdapter;
import com.idoideas.boombox.Adapters.ArtistsAdapter;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.SongsMachine;
import com.idoideas.boombox.R;


/**
 * Created by idoideas on 6/30/16.
 */
public class ArtistsFragment extends android.support.v4.app.Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.songs_fragment, container, false);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(MainView.getContext());
        rv.setLayoutManager(llm);
        ArtistsAdapter adapter = new ArtistsAdapter(SongsMachine.getArtists());
        rv.setAdapter(adapter);
        return view;
    }

}
