package com.idoideas.boombox.Fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.idoideas.boombox.Adapters.PlaylistsAdapter;
import com.idoideas.boombox.Adapters.PlaylistsSelectionAdapter;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.Objects.SongsMachine;
import com.idoideas.boombox.R;

import java.util.ArrayList;


/**
 * Created by idoideas on 6/30/16.
 */
public class PlaylistsSelectionFragment extends android.support.v4.app.Fragment {

    Song song;
    ArrayList<Song> listOfSongs;
    public PlaylistsSelectionFragment(Song song){
        this.song = song;
    }
    public PlaylistsSelectionFragment(ArrayList<Song> listOfSongs){
        this.listOfSongs = listOfSongs;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.songs_fragment, container, false);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(MainView.getContext());
        rv.setLayoutManager(llm);
        PlaylistsSelectionAdapter adapter;
        if (song!=null) {
            adapter = new PlaylistsSelectionAdapter(SongsMachine.getPlaylists(), song);
        }
        else{
            adapter = new PlaylistsSelectionAdapter(SongsMachine.getPlaylists(), listOfSongs);
        }
        rv.setAdapter(adapter);
        return view;
    }

}
