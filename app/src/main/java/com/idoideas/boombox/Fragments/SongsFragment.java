package com.idoideas.boombox.Fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.idoideas.boombox.Adapters.SongsAdapter;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.Objects.SongsMachine;
import com.idoideas.boombox.R;

import java.util.ArrayList;


/**
 * Created by idoideas on 6/30/16.
 */
public class SongsFragment extends android.support.v4.app.Fragment {

    private ArrayList<Song> songs;
    public SongsFragment(ArrayList<Song> songs){
        this.songs = songs;
        MainView.lastList = songs;
    }
    public SongsFragment(){
        this.songs = MainView.lastList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.songs_fragment, container, false);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(MainView.getContext());
        rv.setLayoutManager(llm);
        SongsAdapter adapter = new SongsAdapter(songs);
        rv.setAdapter(adapter);
        return view;
    }

}
