package com.idoideas.boombox.Fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.idoideas.boombox.Adapters.SongsAdapter;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.Objects.SongsMachine;
import com.idoideas.boombox.R;

import java.util.ArrayList;


/**
 * Created by idoideas on 6/30/16.
 */
public class SearchFragment extends android.support.v4.app.Fragment {

    private static ArrayList<Song> results = new ArrayList<>();
    private static View view;

    public static void setQuery(String query){
        results = SongsMachine.searchSongsTitle(query);
        setSongsInList(results);
    }

    static void setSongsInList(ArrayList<Song> songs){
        if(view!=null){
            RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
            SongsAdapter adapter = new SongsAdapter(songs);
            rv.setAdapter(adapter);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.songs_fragment, container, false);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(MainView.getContext());
        rv.setLayoutManager(llm);
        SongsAdapter adapter = new SongsAdapter(results);
        rv.setAdapter(adapter);
        return view;
    }

}
