package com.idoideas.boombox.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import com.idoideas.boombox.Objects.JukeBox;
import com.idoideas.boombox.Objects.MediaNotificationCenter;

/**
 * Created by idoideas on 9/6/16.
 */
public class JukeBoxService extends Service {

    public enum JukeBoxActions{
        JUKEBOX_PLAY,
        JUKEBOX_PAUSE,
        JUKEBOX_NEXT,
        JUKEBOX_PREV,
        JUKEBOX_STOP
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        executeAction(intent.getAction());
        return super.onStartCommand(intent, flags, startId);
    }

    private void executeAction(String func){
        switch(func){
            case "JUKEBOX_PLAY":
                JukeBox.resume();
                break;
            case "JUKEBOX_PAUSE":
                JukeBox.pause();
                break;
            case "JUKEBOX_NEXT":
                JukeBox.skipToNext();
                break;
            case "JUKEBOX_PREV":
                JukeBox.skipToPrevious();
                break;
            case "JUKEBOX_STOP":
                JukeBox.stop();
                break;
            default:
                break;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
