package com.idoideas.boombox.Cast;

import android.media.session.PlaybackState;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.images.WebImage;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.JukeBox;
import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.Objects.SongsMachine;
import com.idoideas.boombox.WebServer.WebServer;
import com.idoideas.boombox.WebServer.WebServerHandler;

import static com.idoideas.boombox.WebServer.WebServerHandler.ServerState.SERVER_CREATED;
import static com.idoideas.boombox.WebServer.WebServerHandler.ServerState.SERVER_NULL;

/**
 * Created by Shayevitz on 12/10/2016.
 */

public class CastManager {

    private static CastSession mCastSession;
    private static SessionManagerListener<CastSession> mSessionManagerListener = setupCastListener();
    private static boolean justDisconnected = false;

    public static void init(){
        mCastSession = getCastContext().getSessionManager().getCurrentCastSession();
        getCastContext().getSessionManager().addSessionManagerListener( mSessionManagerListener, CastSession.class);
    }

    private static SessionManagerListener<CastSession> setupCastListener() {
        return new SessionManagerListener<CastSession>() {

            @Override
            public void onSessionEnded(CastSession session, int error) {
                setJustDisconnected(true);
                onApplicationDisconnected();
            }

            @Override
            public void onSessionResumed(CastSession session, boolean wasSuspended) {
                onApplicationConnected(session);
            }

            @Override
            public void onSessionResumeFailed(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarted(CastSession session, String sessionId) {
                Log.w("cast", "connected!1");
                onApplicationConnected(session);
            }

            @Override
            public void onSessionStartFailed(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarting(CastSession session) {}

            @Override
            public void onSessionEnding(CastSession session) {
                Log.w("cast", "ending");
            }

            @Override
            public void onSessionResuming(CastSession session, String sessionId) {}

            @Override
            public void onSessionSuspended(CastSession session, int reason) {}

            private void onApplicationConnected(final CastSession castSession) {

                Log.w("cast", WebServerHandler.getState().toString());

                if(WebServerHandler.getState()== SERVER_NULL){
                    WebServerHandler.createWebServer(5555);
                    WebServerHandler.startWebServer();
                    Toast.makeText(MainView.getContext(), "Google Cast requires the local server. Server has been enabled.", Toast.LENGTH_SHORT).show();
                }

                mCastSession = castSession;

                //Pushes Queue on completion
                castSession.getRemoteMediaClient().addListener(new RemoteMediaClient.Listener() {

                    boolean toPushTheQueue;
                    @Override
                    public void onStatusUpdated() {
                        if (mCastSession.getRemoteMediaClient().getMediaStatus()!=null) {
                            switch (mCastSession.getRemoteMediaClient().getMediaStatus().getPlayerState()){
                                case MediaStatus.IDLE_REASON_FINISHED:
                                    if (toPushTheQueue) {
                                        JukeBox.skipToNext();
                                        toPushTheQueue = false;
                                    }
                                    break;
                                case MediaStatus.PLAYER_STATE_PLAYING:
                                    toPushTheQueue = true;
                                default:
                                    break;
                            }
                        }
                    }

                    @Override
                    public void onMetadataUpdated() {

                    }

                    @Override
                    public void onQueueStatusUpdated() {

                    }

                    @Override
                    public void onPreloadStatusUpdated() {

                    }

                    @Override
                    public void onSendingRemoteMediaRequest() {

                    }
                });

                //Updates device of position
                castSession.getRemoteMediaClient().addProgressListener(new RemoteMediaClient.ProgressListener() {
                    @Override
                    public void onProgressUpdated(long l, long l1) {
                        JukeBox.seekTo((int)l);
                    }
                },10);
                if (JukeBox.getCurrentSong()!=null) {

                    if (JukeBox.isPlaying()) {
                        //JukeBox.pause();
                        castSong(JukeBox.getCurrentSong(), JukeBox.getCurrentPosition());
                    }
                }
            }

            private void onApplicationDisconnected() {

            }
        };
    }

    public static void setJustDisconnected(boolean bool){
        justDisconnected = bool;
    }

    public static boolean isJustDisconnected(){
        return justDisconnected;
    }

    public static CastContext getCastContext(){
        return CastContext.getSharedInstance(MainView.getThis());
    }

    public static boolean isCasting(){
        return (mCastSession != null)
                && (mCastSession.isConnected() || mCastSession.isConnecting());
    }

    public static void castSong(Song song, int position) {
        if (mCastSession == null) {
            return;
        }
        final RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
        if (remoteMediaClient == null) {
            return;
        }
        remoteMediaClient.load(songToMediaInfo(song), true, position);

    }

    private static MediaInfo songToMediaInfo(Song song) {
        MediaMetadata songMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MUSIC_TRACK);

        String songURL = "http://"+WebServerHandler.getAddress()+"/"+song.getId()+"?allow=1";
        songMetadata.putString(MediaMetadata.KEY_ARTIST, song.getArtist());
        songMetadata.putString(MediaMetadata.KEY_ALBUM_TITLE, song.getAlbumTitle());
        songMetadata.putString(MediaMetadata.KEY_TITLE, song.getTitle());
        songMetadata.addImage(new WebImage(Uri.parse(songURL+"&image=1")));

        Log.w("address", songURL);
        return new MediaInfo.Builder(songURL)
                .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
                .setContentType("audio/mp3")
                .setMetadata(songMetadata)
                .setStreamDuration((long)song.getLength())
                .build();
    }

}
