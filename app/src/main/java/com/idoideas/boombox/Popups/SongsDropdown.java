package com.idoideas.boombox.Popups;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.idoideas.boombox.Fragments.PlaylistsFragment;
import com.idoideas.boombox.Fragments.PlaylistsSelectionFragment;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.JukeBox;
import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.Objects.SongsMachine;

import java.lang.reflect.Method;
import java.util.function.Function;

/**
 * Created by idoideas on 8/24/16.
 */
public class SongsDropdown extends PopupMenu {
    public SongsDropdown(@NonNull Context context, @NonNull final View anchor, final Song song) {
        super(context, anchor);
        this.getMenu().add(1, 1, 1, "Add to playlist");
        this.getMenu().add(2, 2, 2, "Add to queue");
        this.getMenu().add(3, 3, 3, "Play next");
        this.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Log.w("Item", ""+item.getOrder());
                switch (item.getOrder()){
                    case 1:
                        MainView.replaceFragment(new PlaylistsSelectionFragment(song));
                        break;
                    case 2:
                        JukeBox.addToQueue(song);
                        Snackbar.make(MainView.getView(),"\""+song.getTitle()+"\" was added to Queue.", Snackbar.LENGTH_SHORT).show();
                        break;
                    case 3:
                        JukeBox.playNext(song);
                        Snackbar.make(MainView.getView(),"\""+song.getTitle()+"\" was set to be played next.", Snackbar.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });
    }


}
