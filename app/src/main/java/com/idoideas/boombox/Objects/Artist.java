package com.idoideas.boombox.Objects;

import android.content.ContentUris;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v4.content.ContextCompat;

import com.idoideas.boombox.MainView;
import com.idoideas.boombox.R;

import java.io.FileDescriptor;
import java.util.ArrayList;

/**
 * Created by idoideas on 6/30/16.
 */
public class Artist {
    private String name;
    private ArrayList<Song> artistSongs = new ArrayList<>();
    private int id;

    public Artist(String name, int id){
        if(name.equals("<unknown>")){
            this.name = "Unknown Artist";
        }
        else{
            this.name = name;
        }
        this.id = id;
    }

    //YOU SHOULDNT USE IT RATHER THAN IN SONGSMACHINE
    public void addSong(Song song){
        this.artistSongs.add(song);
    }

    public Song getSong(int i){
        return artistSongs.get(i);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Song> getArtistSongs() {
        return artistSongs;
    }

    public void setArtistSongs(ArrayList<Song> artistSongs) {
        this.artistSongs = artistSongs;
    }

    public double getTotalLength() {
        int size = getArtistSongs().size();
        double totalLength = 0;
        for(int i = 0; i<size; i++){
            totalLength += getSong(i).getLength();
        }
        return totalLength;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
