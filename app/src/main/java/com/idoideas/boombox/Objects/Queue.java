package com.idoideas.boombox.Objects;

import java.util.ArrayList;

/**
 * Created by Shayevitz on 11/02/2017.
 */

public class Queue {

    private Playlist queueList;
    private int index = 0;
    public Queue(Playlist queue){
        queueList = queue;
    }
    public Song getCurrentSong(){
        return queueList.getSong(index);
    }
    public ArrayList<Song> getQueueSongs(){
        return queueList.getAllSongs();
    }
    public void add(Song song){
        queueList.add(song);
    }
    public void add(Song song, int i){
        queueList.add(song, i);
    }
    public void removeCurrentSong(){
        queueList.remove(queueList.getSong(index));
    }
    public boolean isEmpty(){
        return queueList.isEmpty();
    }
    public int getIndex(){
        return index;
    }
    public void setIndex(int i){
        index = i;
    }
    Song getSong(int p){
        return queueList.getSong(p);
    }
    void shiftIndexForward(){
        if(queueList.getAllSongs().size()!=index){
            index++;
        }
    }
    void shiftIndexBack() {
        if(index!=0){
            index--;
        }
    }
}
