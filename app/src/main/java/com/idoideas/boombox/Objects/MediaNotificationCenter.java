package com.idoideas.boombox.Objects;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.MediaMetadataRetriever;
import android.media.RemoteControlClient;
import android.media.RemoteController;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.os.Build;
import android.os.RemoteException;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.app.NotificationCompat;

import com.idoideas.boombox.Cast.CastManager;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.R;
import com.idoideas.boombox.Services.JukeBoxService;
import com.idoideas.boombox.Services.JukeBoxService.JukeBoxActions;

/**
 * Created by idoideas on 9/3/16.
 */
public class MediaNotificationCenter {
    private static MediaSessionCompat mSession = new MediaSessionCompat(MainView.getContext(), "BoomBox Notification");
    private static MediaControllerCompat mController = initController();

    public static void postMediaNotification(){
        send(getNotification(JukeBox.getQueueSong(0)));
    }


    private static void send(android.support.v4.app.NotificationCompat.Builder builder){
        NotificationManager notificationManager = (NotificationManager) MainView.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
    }



    private static android.support.v4.app.NotificationCompat.Builder getNotification(Song song){
        Context context = MainView.getContext();
        Intent intent = new Intent(MainView.getThis(), MainView.class);
        android.support.v4.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setStyle(new NotificationCompat.MediaStyle().setMediaSession(mSession.getSessionToken()))
                .setSmallIcon(R.drawable.boombox_drawing_placeholder)
                .setContentTitle(song.getTitle())
                .setContentText(song.getArtist())
                .setLargeIcon(song.getAlbumArt())

                .setContentIntent(PendingIntent.getActivity(context, 2, intent, PendingIntent.FLAG_CANCEL_CURRENT))
                .setColor(context.getResources().getColor(R.color.colorAccent));

        builder.addAction(generateAction(android.R.drawable.ic_media_previous, "Previous", JukeBoxActions.JUKEBOX_PREV, 0));
        if(JukeBox.isPlaying()){
            builder.addAction(generateAction(android.R.drawable.ic_media_pause, "Pause", JukeBoxActions.JUKEBOX_PAUSE, 1));
            builder.setOngoing(true);
            builder.setAutoCancel(false);
        }
        else{
            builder.addAction(generateAction(android.R.drawable.ic_media_play, "Play", JukeBoxActions.JUKEBOX_PLAY, 2));
        }
        builder.addAction(generateAction(android.R.drawable.ic_media_next, "Next", JukeBoxActions.JUKEBOX_NEXT, 3));

        return builder;
    }

    public static MediaControllerCompat.TransportControls getMediaControls(){
        if(mController==null){
            mController = initController();
        }
        return mController.getTransportControls();
    }

    public static MediaControllerCompat initController(){
        try {
            initMediaSession();
            return new MediaControllerCompat(MainView.getContext(), mSession.getSessionToken() );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void initMediaSession(){
        if(mSession!=null){
            mSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
            mSession.setCallback( new MediaSessionCompat.Callback() {
                @Override
                public void onPlay() {
                    super.onPlay();
                    if (CastManager.isJustDisconnected()){
                        CastManager.setJustDisconnected(false);
                    }
                    else{
                        MediaNotificationCenter.postMediaNotification();
                        mSession.setMetadata(new MediaMetadataCompat.Builder()
                                .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, JukeBox.getQueueSong(0).getAlbumArt()).build());
                    }
                }

                @Override
                public void onPause() {
                    super.onPause();
                    MediaNotificationCenter.postMediaNotification();
                }

                @Override
                public void onSkipToNext() {
                    super.onSkipToNext();
                    //Change media here
                    MediaNotificationCenter.postMediaNotification();
                }

                @Override
                public void onSkipToPrevious() {
                    super.onSkipToPrevious();
                    //Change media here
                    MediaNotificationCenter.postMediaNotification();
                }

                @Override
                public void onFastForward() {
                    super.onFastForward();
                    //Manipulate current media here
                }

                @Override
                public void onRewind() {
                    super.onRewind();
                    //Manipulate current media here
                }

                @Override
                public void onStop() {
                    super.onStop();
                    NotificationManager notificationManager = (NotificationManager) MainView.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancel( 1 );
                }

                @Override
                public void onSeekTo(long pos) {
                    super.onSeekTo(pos);
                    JukeBox.seekTo((int)pos);
                }

            });
            mSession.setActive(true);
        }
    }


    private static android.support.v4.app.NotificationCompat.Action generateAction(int icon, String title, JukeBoxActions action, int id) {
        Intent intent = new Intent( MainView.getContext(), JukeBoxService.class );
        intent.setAction(action.toString());
        PendingIntent pendingIntent = PendingIntent.getService(MainView.getContext(), id, intent, 0);
        return new NotificationCompat.Action.Builder( icon, title, pendingIntent ).build();
    }

}
