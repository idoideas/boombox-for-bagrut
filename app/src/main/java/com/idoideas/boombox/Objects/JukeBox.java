package com.idoideas.boombox.Objects;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.PowerManager;
import android.util.Log;

import com.idoideas.boombox.Cast.CastManager;
import com.idoideas.boombox.MainView;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by idoideas on 7/16/16.
 */
public class JukeBox {

    public enum PlaybackLocation {
        LOCAL,
        CAST
    }

    private static MediaPlayer player = new MediaPlayer();
    private static Queue queue = new Queue(new Playlist("Queue"));
    private static AudioManager mAudioManager;
    private static AudioManager.OnAudioFocusChangeListener foucsChangeListener;

    public static void init(){
        player.setWakeMode(MainView.getContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        foucsChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int i) {
                if(i<=0) {
                    pause();
                } else {
                    resume();
                }
            }
        };
        mAudioManager = (AudioManager) MainView.getThis().getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.requestAudioFocus(foucsChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                JukeBox.onPrepared(mediaPlayer);
            }
        });
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                JukeBox.onCompletion(mediaPlayer);
            }
        });
        player.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return JukeBox.onError(mediaPlayer, i , i1);
            }
        });
    }

    public static void play(Song song){
        if(!queue.isEmpty()) {
            queue.removeCurrentSong();
        }
        queue.add(song, queue.getIndex());
        MainView.setSongDetails(song);
        Log.w("Casting", String.valueOf(CastManager.isCasting()));
        if(CastManager.isCasting()){
            CastManager.castSong(song, 0);
        }
        else {
            preparePlayer(song);
        }
        MediaNotificationCenter.getMediaControls().play();
    }

    private static void playFromQueue(){
        Song song = queue.getCurrentSong();
        MainView.setSongDetails(song);
        if(CastManager.isCasting()){
            CastManager.castSong(song, 0);
        }
        else {
            preparePlayer(song);
        }
        MediaNotificationCenter.getMediaControls().play();
    }

    private static void preparePlayer(Song song){
        try {
            player.reset();
            player.setDataSource(song.getPath());
            player.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void pause(){
        if (player.isPlaying()){
            player.pause();
            MediaNotificationCenter.getMediaControls().pause();
        }
    }

    public static void resume(){
        if (!player.isPlaying() && player.getCurrentPosition()>1){
            player.start();
            MediaNotificationCenter.getMediaControls().play();
        }
    }

    public static void stop(){
        player.stop();
        MediaNotificationCenter.getMediaControls().stop();
    }

    public static void skipToNext(){
        player.stop();
        queue.shiftIndexForward();
        playFromQueue();
    }

    public static void skipToPrevious(){
        player.stop();
        queue.shiftIndexBack();
        playFromQueue();
    }

    public static void addToQueue(Song song){
        queue.add(song);
    }

    public static void playNext(Song song){
        queue.add(song,1);
    }

    public static ArrayList<Song> getQueueSongs(){
        return queue.getQueueSongs();
    }

    public static Song getQueueSong(int position){
        return queue.getSong(position);
    }

    public static Song getCurrentSong(){
        if (queue.isEmpty()){
            return null;
        }
        return queue.getSong(0);
    }

    public static int getCurrentPosition(){
        return player.getCurrentPosition();
        //returns in milliseconds
    }

    public static int getCurrentDuration(){
        return player.getDuration();
        //returns in milliseconds
    }


    public static void seekTo(int millisecond){
        player.seekTo(millisecond);
    }

    public static void seekTo(int minute, int second){
        player.seekTo(minute*60000+second*1000);
    }

    public static boolean isPlaying(){
        return player.isPlaying();
    }

    //What happens when player completes playing?
    public static void onCompletion(MediaPlayer mediaPlayer) {
        skipToNext();
    }


    //What happens when player is having an error?
    public static boolean onError(MediaPlayer mediaPlayer, int i, int i1) {

        return false;
    }

    //What happens when player is prepared?
    public static void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        MediaNotificationCenter.getMediaControls().play();
    }

    public static void abandonAudioFocus(){
        mAudioManager.abandonAudioFocus(foucsChangeListener);
    }

}
