package com.idoideas.boombox.Objects;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.idoideas.boombox.MainView;
import com.idoideas.boombox.R;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by idoideas on 6/30/16.
 */
public class Song {
    private String title, artist, path, albumTitle;
    private long length;
    private long albumArtID;
    private Uri albumUri;
    //private Album album;
    private int id;


    public Song(String title, String artist, String path, long length, long albumArtID, String album, int id){
        this.title = title;
        if(artist.equals("<unknown>")){
            this.artist = "Unknown Artist";
        }
        else{
            this.artist = artist;
        }
        this.id = id;
        this.path = path;
        this.length = length;
        this.albumArtID = albumArtID;
        this.albumUri = getAlbumArtURI(albumArtID);
        this.albumTitle = album;

    }

    private Uri getAlbumArtURI(long album_id){
        final Uri sArtworkUri = Uri
                .parse("content://media/external/audio/albumart");

        return ContentUris.withAppendedId(sArtworkUri, album_id);
    }

    //Gets Bitmap from album ID
    //TODO: PUT A CREATIVE "NO-IMAGE" INSTEAD OF PLACEHOLDER
    //TODO: FIX ALBUM ART DISPLAY
    public Bitmap getBitmapAlbumArt(Uri uri)
    {
        Bitmap bm = null;
        try
        {

            ParcelFileDescriptor pfd = MainView.getContext().getContentResolver()
                    .openFileDescriptor(uri, "r");

            if (pfd != null)
            {
                FileDescriptor fd = pfd.getFileDescriptor();
                bm = BitmapFactory.decodeFileDescriptor(fd);
            }
            else{
                //TODO: CHANGE THAT DEFAULT IMAGE
                this.albumArtID = -1;
                Drawable drawable = ContextCompat.getDrawable(MainView.getContext(), R.drawable.album_not_found_placeholder);
                bm =  convertToBitmap(drawable, 500, 500);
            }
        } catch (Exception e) {
            //TODO: CHANGE THAT DEFAULT IMAGE
            this.albumArtID = -1;
            Drawable drawable = ContextCompat.getDrawable(MainView.getContext(), R.drawable.album_not_found_placeholder);
            return convertToBitmap(drawable, 510, 510);
        }
        return bm;
    }

    public static Bitmap convertToBitmap(Drawable drawable, int widthPixels, int heightPixels) {
        Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, widthPixels, heightPixels);
        drawable.draw(canvas);

        return mutableBitmap;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public void setAlbumArtID(long albumArtID) {
        this.albumArtID = albumArtID;
        this.albumUri = getAlbumArtURI(albumArtID);
    }


    public boolean hasAlbumArt() {
        return this.albumArtID!=-1;
    }

    public Bitmap getAlbumArt() {
        return getBitmapAlbumArt(getAlbumUri());
    }

    public Bitmap getIconAlbumArt(){
        return Bitmap.createScaledBitmap(getAlbumArt(), 50, 50, true);
    }

    public long getAlbumArtID() {
        return albumArtID;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public String getPath() {
        return path;
    }

    public double getLength() {
        return length;
    }

    public Uri getAlbumUri() {
        return albumUri;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public void setAlbumUri(Uri albumUri) {
        this.albumUri = albumUri;
    }

    /*public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
