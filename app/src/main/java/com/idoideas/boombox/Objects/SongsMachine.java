package com.idoideas.boombox.Objects;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Resources.UriInOut;


import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by idoideas on 6/30/16.
 */
public class SongsMachine {
    private static ArrayList<Song> songs = getAllTheSongs();
    private static ArrayList<Album> albums = getAllTheAlbums();
    private static ArrayList<Artist> artists = getAllTheArtists();
    private static ArrayList<Playlist> playlists = getAllThePlaylists();

    //Gets all the songs, obviously.
    //As an ArrayList, though.
    private static ArrayList<Song> getAllTheSongs() {
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        String[] projection = {
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.ALBUM
        };
        final String sortOrder = MediaStore.Audio.AudioColumns.TITLE + " COLLATE LOCALIZED ASC";
        ArrayList<Song> SongsList = new ArrayList<>();
        Cursor cursor = null;
        try {
            Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            cursor = MainView.getContext().getContentResolver().query(uri, projection, selection, null, sortOrder);
            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    SongsList.add(cursorToSong(cursor, SongsList.size()));
                    cursor.moveToNext();
                }

            }

        } catch (Exception e) {
            Log.e("EXCEPTION:", e.toString(), e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return SongsList;
    }

    //Gets all the albums, obviously.
    //As an ArrayList, though.
    //TODO: I CAN MAKE THIS MORE EFFICIENT
    private static ArrayList<Album> getAllTheAlbums() {
        ArrayList<Album> albumsList = new ArrayList<>();
        for(int i = 0; i<songs.size(); i++){
            if(!isAlbumInList(songs.get(i).getAlbumArtID(), albumsList)){
                Album currentAlbum = new Album(songs.get(i).getAlbumTitle(), songs.get(i).getArtist(), songs.get(i).getAlbumArtID(), albumsList.size());
                for(int j = i; j<songs.size(); j++){
                    if (songs.get(j).getAlbumArtID()==currentAlbum.getAlbumArtID()){
                        //songs.get(j).setAlbum(currentAlbum);
                        currentAlbum.addSong(songs.get(j));
                        if (!currentAlbum.getArtist().equals("Various Artists")){
                            if (!currentAlbum.getArtist().equals(songs.get(j).getArtist())){
                                currentAlbum.setArtist("Various Artists");
                            }
                        }

                    }
                }
                albumsList.add(currentAlbum);
            }
        }
        return albumsList;
    }

    //Gets all the albums, obviously.
    //As an ArrayList, though.
    //TODO: I CAN MAKE THIS MORE EFFICIENT
    private static ArrayList<Artist> getAllTheArtists() {
        ArrayList<Artist> artistsList = new ArrayList<>();
        for(int i = 0; i<songs.size(); i++){
            if(!isArtistInList(songs.get(i).getArtist(), artistsList)){
                Artist currentArtist = new Artist(songs.get(i).getArtist(), artistsList.size());
                for(int j = i; j<songs.size(); j++){
                    if (songs.get(j).getArtist().equals(currentArtist.getName())){
                        //songs.get(j).setAlbum(currentAlbum);
                        currentArtist.addSong(songs.get(j));
                    }
                }
                artistsList.add(currentArtist);
            }
        }
        return artistsList;
    }

    private static ArrayList<Playlist> getAllThePlaylists(){
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Uri.class, new UriInOut())
                .create();
        String json = MainView.getPrefrences().getString("playlists", "");
        Playlist[] array = gson.fromJson(json,Playlist[].class);
        if (array==null || array.length==0){
            return new ArrayList<>();
        }
        return new ArrayList<>(Arrays.asList(array));
    }

    public static void saveAllThePlaylists(){
        SharedPreferences.Editor prefsEditor = MainView.getPrefrences().edit();
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Uri.class, new UriInOut())
                .create();
        String json = gson.toJson(playlists);
        prefsEditor.putString("playlists", json);
        prefsEditor.apply();
    }

    public static ArrayList<Song> searchSongsTitle(String query){
        ArrayList<Song> resultsTemp = new ArrayList<>();
        for(int i = 0; i< SongsMachine.getSongsNumber(); i++){
            if(SongsMachine.getSong(i).getTitle().toLowerCase().contains(query.toLowerCase())){
                resultsTemp.add(SongsMachine.getSong(i));
            }
        }
        return resultsTemp;
    }

    public static ArrayList<Song> searchSongsPath(String query){
        ArrayList<Song> resultsTemp = new ArrayList<>();
        for(int i = 0; i< SongsMachine.getSongsNumber(); i++){
            if(SongsMachine.getSong(i).getPath().toLowerCase().contains(query.toLowerCase())){
                resultsTemp.add(SongsMachine.getSong(i));
            }
        }
        return resultsTemp;
    }

    public static ArrayList<Album> searchAlbumsTitle(String query){
        ArrayList<Album> resultsTemp = new ArrayList<>();
        for(int i = 0; i< SongsMachine.getAlbumsNumber(); i++){
            if(SongsMachine.getAlbum(i).getTitle().toLowerCase().contains(query.toLowerCase())){
                resultsTemp.add(SongsMachine.getAlbum(i));
            }
        }
        return resultsTemp;
    }

    public static ArrayList<Artist> searchArtistsName(String query){
        ArrayList<Artist> resultsTemp = new ArrayList<>();
        for(int i = 0; i< SongsMachine.getArtistsNumber(); i++){
            if(SongsMachine.getArtist(i).getName().toLowerCase().contains(query.toLowerCase())){
                resultsTemp.add(SongsMachine.getArtist(i));
            }
        }
        return resultsTemp;
    }

    public static ArrayList<Playlist> searchPlaylistsTitle(String query){
        ArrayList<Playlist> resultsTemp = new ArrayList<>();
        for(int i = 0; i< SongsMachine.getPlaylistsNumber(); i++){
            if(SongsMachine.getPlaylist(i).getTitle().toLowerCase().contains(query.toLowerCase())){
                resultsTemp.add(SongsMachine.getPlaylist(i));
            }
        }
        return resultsTemp;
    }

    public static void addPlaylist(Playlist playlist){
        playlists.add(playlist);
        saveAllThePlaylists();
    }

    public static void removePlaylist(Playlist playlist){
        playlists.remove(playlist);
        saveAllThePlaylists();
    }

    public static ArrayList<Playlist> getPlaylists(){
        return playlists;
    }

    public static int getPlaylistsNumber(){
        return playlists.size();
    }

    public static Playlist getPlaylist(int i){
        return playlists.get(i);
    }

    private static boolean isAlbumInList(long id, ArrayList<Album> albums){
        for(Album album : albums){
            if(album.getAlbumArtID()==id){
                return true;
            }
        }
        return false;
    }

    private static boolean isArtistInList(String name, ArrayList<Artist> artists){
        for(Artist artist : artists){
            if(artist.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    //Gets a media cursor
    //and turns it into a Song object.
    private static Song cursorToSong(Cursor c, int id){
        return new Song(c.getString(0), c.getString(1), c.getString(2), c.getLong(3), c.getLong(4), c.getString(5), id);
    }

    //Gets the full songs list, to outer classes.
    public static ArrayList<Song> getSongs(){
        return songs;
    }

    //Gets songs list number
    public static int getSongsNumber(){
        return songs.size();
    }

    //Get a specific song.
    public static Song getSong(int id){
        return songs.get(id);
    }

    //Gets the full songs list, to outer classes.
    public static ArrayList<Album> getAlbums(){
        return albums;
    }

    //Gets songs list number
    public static int getAlbumsNumber(){
        return albums.size();
    }

    //Get a specific song.
    public static Album getAlbum(int id){
        return albums.get(id);
    }

    public static Artist getArtist(int id){
        return artists.get(id);
    }

    public static ArrayList<Artist> getArtists(){
        return artists;
    }

    public static int getArtistsNumber(){
        return artists.size();
    }

}
