package com.idoideas.boombox.Objects;

import android.content.ContentUris;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v4.content.ContextCompat;

import com.idoideas.boombox.MainView;
import com.idoideas.boombox.R;

import java.io.FileDescriptor;
import java.util.ArrayList;

/**
 * Created by idoideas on 6/30/16.
 */
public class Album {
    private String title, artist;
    private long albumArtID;
    private Uri albumUri;
    private ArrayList<Song> albumSongs = new ArrayList<>();
    private int id;

    public Album(String title, String artist, long albumArtID, int id){
        this.title = title;
        if(artist.equals("<unknown>")){
            this.artist = "";
        }
        else{
            this.artist = artist;
        }
        this.albumArtID = albumArtID;
        this.albumUri = getAlbumArtURI(albumArtID);
        this.id = id;
    }

    //YOU SHOULDNT USE IT RATHER THAN IN SONGSMACHINE
    public void addSong(Song song){
        this.albumSongs.add(song);
    }

    public Song getSong(int i){
        return albumSongs.get(i);
    }
    private Uri getAlbumArtURI(long album_id){
        final Uri sArtworkUri = Uri
                .parse("content://media/external/audio/albumart");

        return ContentUris.withAppendedId(sArtworkUri, album_id);
    }

    //Gets Bitmap from album ID
    //TODO: PUT A CREATIVE "NO-IMAGE" INSTEAD OF PLACEHOLDER
    //TODO: FIX ALBUM ART DISPLAY
    public Bitmap getBitmapAlbumArt(Uri uri)
    {
        Bitmap bm = null;
        try
        {

            ParcelFileDescriptor pfd = MainView.getContext().getContentResolver()
                    .openFileDescriptor(uri, "r");

            if (pfd != null)
            {
                FileDescriptor fd = pfd.getFileDescriptor();
                bm = BitmapFactory.decodeFileDescriptor(fd);
            }
            else{
                //TODO: CHANGE THAT DEFAULT IMAGE
                this.albumArtID = -1;
                Drawable drawable = ContextCompat.getDrawable(MainView.getContext(), R.drawable.album_not_found_placeholder);
                bm =  convertToBitmap(drawable, 500, 500);
            }
        } catch (Exception e) {
            //TODO: CHANGE THAT DEFAULT IMAGE
            this.albumArtID = -1;
            Drawable drawable = ContextCompat.getDrawable(MainView.getContext(), R.drawable.album_not_found_placeholder);
            return convertToBitmap(drawable, 510, 510);
        }
        return bm;
    }

    public static Bitmap convertToBitmap(Drawable drawable, int widthPixels, int heightPixels) {
        Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, widthPixels, heightPixels);
        drawable.draw(canvas);

        return mutableBitmap;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setAlbumArtID(long albumArtID) {
        this.albumArtID = albumArtID;
        this.albumUri = getAlbumArtURI(albumArtID);
    }


    public boolean hasAlbumArt() {
        return this.albumArtID!=-1;
    }

    public Bitmap getAlbumArt() {
        return getBitmapAlbumArt(getAlbumUri());
    }

    public long getAlbumArtID() {
        return albumArtID;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public double getTotalLength() {
        int size = getAlbumSongs().size();
        double totalLength = 0;
        for(int i = 0; i<size; i++){
            totalLength += getSong(i).getLength();
        }
        return totalLength;
    }

    public ArrayList<Song> getAlbumSongs() {
        return albumSongs;
    }

    public void setAlbumSongs(ArrayList<Song> albumSongs) {
        this.albumSongs = albumSongs;
    }

    public Uri getAlbumUri() {
        return albumUri;
    }

    public void setAlbumUri(Uri albumUri) {
        this.albumUri = albumUri;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
