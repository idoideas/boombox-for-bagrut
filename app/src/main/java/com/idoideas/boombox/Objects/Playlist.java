package com.idoideas.boombox.Objects;

import java.util.ArrayList;

/**
 * Created by idoideas on 8/20/16.
 */
public class Playlist {
    private ArrayList<Song> playlistSongs;
    private String title;

    public Playlist(String title){
        playlistSongs = new ArrayList<>();
        this.title = title;
    }

    public void add(Song song){
        playlistSongs.add(song);
        save();
    }

    public void add(Song song, int index){
        playlistSongs.add(index, song);
        save();
    }

    public void addList(ArrayList<Song> songList){
        for(Song song : songList){
            this.add(song);
        }
    }

    public void remove(Song song){
        playlistSongs.remove(song);
        save();
    }

    public Song getSong(int i){
        return playlistSongs.get(i);
    }

    public ArrayList<Song> getAllSongs(){
        return playlistSongs;
    }

    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title){
        this.title = title;
        save();
    }

    public boolean isEmpty(){
        return playlistSongs.isEmpty();
    }

    public void empty(){
        playlistSongs.clear();
    }

    public void save(){
        SongsMachine.saveAllThePlaylists();
    }
}
