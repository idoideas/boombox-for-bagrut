package com.idoideas.boombox.Adapters;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.idoideas.boombox.Fragments.SongsFragment;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.Artist;
import com.idoideas.boombox.Objects.Artist;
import com.idoideas.boombox.R;

import java.util.List;

/**
 * Created by idoideas on 7/11/16.
 */
public class ArtistsAdapter extends RecyclerView.Adapter<ArtistsAdapter.ArtistViewHolder>{

    private List<Artist> artists;

    //general constructor
    public ArtistsAdapter(List<Artist> artists){
        this.artists = artists;
    }

    //creates number of items as returned
    @Override
    public int getItemCount() {
        return artists.size();
    }

    //creates view according to layout
    @Override
    public ArtistViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.songs_item, viewGroup, false);
        return new ArtistViewHolder(v);
    }

    //sets values to view
    @Override
    public void onBindViewHolder(ArtistViewHolder personViewHolder, int i) {
        personViewHolder.artistsName.setText(artists.get(i).getName());
        //personViewHolder.artistsAlbumArt.setImageBitmap(artists.get(i).getArtistArt());
        personViewHolder.artist = artists.get(i);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    //item view holder
    public static class ArtistViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView artistsName;
        ImageView artistsAlbumArt;
        Artist artist;


        ArtistViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            artistsName = (TextView)itemView.findViewById(R.id.songsName);
            //artistsAlbumArt = (ImageView)itemView.findViewById(R.id.songsArt);

            cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, artistsName.getText(), Snackbar.LENGTH_SHORT).show();
                    MainView.replaceFragmentWithTransition(new SongsFragment(artist.getArtistSongs()), true);
                }
            });
        }
    }

}