package com.idoideas.boombox.Adapters;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.idoideas.boombox.Fragments.SongsFragment;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.Playlist;
import com.idoideas.boombox.Objects.Playlist;
import com.idoideas.boombox.R;

import java.util.List;

/**
 * Created by idoideas on 7/11/16.
 */
public class PlaylistsAdapter extends RecyclerView.Adapter<PlaylistsAdapter.PlaylistViewHolder>{

    private List<Playlist> playlists;

    //general constructor
    public PlaylistsAdapter(List<Playlist> playlists){
        this.playlists = playlists;
    }

    //creates number of items as returned
    @Override
    public int getItemCount() {
        return playlists.size();
    }

    //creates view according to layout
    @Override
    public PlaylistViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.songs_item, viewGroup, false);
        return new PlaylistViewHolder(v);
    }

    //sets values to view
    @Override
    public void onBindViewHolder(PlaylistViewHolder personViewHolder, int i) {
        personViewHolder.playlistsName.setText(playlists.get(i).getTitle());
        //personViewHolder.playlistsAlbumArt.setImageBitmap(playlists.get(i).getPlaylistArt());
        personViewHolder.playlist = playlists.get(i);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    //item view holder
    public static class PlaylistViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView playlistsName;
        ImageView playlistsAlbumArt;
        Playlist playlist;


        PlaylistViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            playlistsName = (TextView)itemView.findViewById(R.id.songsName);
            //playlistsAlbumArt = (ImageView)itemView.findViewById(R.id.songsArt);

            cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, playlistsName.getText(), Snackbar.LENGTH_SHORT).show();
                    MainView.replaceFragmentWithTransition(new SongsFragment(playlist.getAllSongs()), true);
                }
            });
        }
    }

}