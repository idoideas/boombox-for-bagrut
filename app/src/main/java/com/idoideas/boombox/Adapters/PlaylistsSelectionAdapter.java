package com.idoideas.boombox.Adapters;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.idoideas.boombox.Fragments.SongsFragment;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.Playlist;
import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.Objects.SongsMachine;
import com.idoideas.boombox.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idoideas on 7/11/16.
 */
public class PlaylistsSelectionAdapter extends RecyclerView.Adapter<PlaylistsSelectionAdapter.PlaylistViewHolder>{

    private List<Playlist> playlists;
    private Song song;
    private ArrayList<Song> listOfSongs;
    //general constructor
    public PlaylistsSelectionAdapter(List<Playlist> playlists, Song song){
        this.playlists = playlists;
        this.song = song;
    }

    public PlaylistsSelectionAdapter(List<Playlist> playlists, ArrayList<Song> listOfSongs){
        this.playlists = playlists;
        this.listOfSongs = listOfSongs;
    }
    //creates number of items as returned
    @Override
    public int getItemCount() {
        return playlists.size();
    }

    //creates view according to layout
    @Override
    public PlaylistViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.songs_item, viewGroup, false);
        return new PlaylistViewHolder(v);
    }

    //sets values to view
    @Override
    public void onBindViewHolder(PlaylistViewHolder personViewHolder, int i) {
        personViewHolder.playlistsName.setText(playlists.get(i).getTitle());
        //personViewHolder.playlistsArtist.setText(playlists.get(i).getArtist());
        //personViewHolder.playlistsArt.setImageBitmap(playlists.get(i).getPlaylistArt());
        personViewHolder.playlist = playlists.get(i);
        personViewHolder.song = song;
        personViewHolder.listOfSongs = listOfSongs;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    //item view holder
    public static class PlaylistViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView playlistsName;
        TextView playlistsArtist;
        ImageView playlistsArt;
        Playlist playlist;
        Song song;
        ArrayList<Song> listOfSongs;


        PlaylistViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            playlistsName = (TextView)itemView.findViewById(R.id.songsName);
            playlistsArtist = (TextView)itemView.findViewById(R.id.songsArtist);
            playlistsArt = (ImageView)itemView.findViewById(R.id.songsArt);

            cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(song!=null){
                        playlist.add(song);
                    }
                    else if(listOfSongs!=null){
                        playlist.addList(listOfSongs);
                    }
                    Snackbar.make(MainView.getView(),"Added to playlist", Snackbar.LENGTH_SHORT).show();
                    MainView.replaceFragmentWithTransition(new SongsFragment(SongsMachine.getSongs()), true);
                }
            });
        }
    }

}