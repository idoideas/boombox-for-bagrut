package com.idoideas.boombox.Adapters;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.idoideas.boombox.Fragments.SongsFragment;
import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.Album;
import com.idoideas.boombox.Objects.JukeBox;
import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.R;

import java.util.List;

/**
 * Created by idoideas on 7/11/16.
 */
public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.AlbumViewHolder>{

    private List<Album> albums;

    //general constructor
    public AlbumsAdapter(List<Album> albums){
        this.albums = albums;
    }

    //creates number of items as returned
    @Override
    public int getItemCount() {
        return albums.size();
    }

    //creates view according to layout
    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.songs_item, viewGroup, false);
        return new AlbumViewHolder(v);
    }

    //sets values to view
    @Override
    public void onBindViewHolder(AlbumViewHolder personViewHolder, int i) {
        personViewHolder.albumsName.setText(albums.get(i).getTitle());
        personViewHolder.albumsArtist.setText(albums.get(i).getArtist());
        personViewHolder.albumsArt.setImageBitmap(albums.get(i).getAlbumArt());
        personViewHolder.album = albums.get(i);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    //item view holder
    public static class AlbumViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView albumsName;
        TextView albumsArtist;
        ImageView albumsArt;
        Album album;


        AlbumViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            albumsName = (TextView)itemView.findViewById(R.id.songsName);
            albumsArtist = (TextView)itemView.findViewById(R.id.songsArtist);
            albumsArt = (ImageView)itemView.findViewById(R.id.songsArt);

            cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, albumsName.getText(), Snackbar.LENGTH_SHORT).show();
                    MainView.replaceFragmentWithTransition(new SongsFragment(album.getAlbumSongs()), true);
                }
            });
        }
    }

}