package com.idoideas.boombox.Adapters;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.JukeBox;
import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.Objects.SongsMachine;
import com.idoideas.boombox.Popups.SongsDropdown;
import com.idoideas.boombox.R;

import java.util.List;

/**
 * Created by idoideas on 7/11/16.
 */
public class SongsAdapter extends RecyclerView.Adapter<SongsAdapter.SongViewHolder>{

    private List<Song> songs;

    //general constructor
    public SongsAdapter(List<Song> songs){
        this.songs = songs;
    }

    //creates number of items as returned
    @Override
    public int getItemCount() {
        return songs.size();
    }

    //creates view according to layout
    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.songs_item, viewGroup, false);
        return new SongViewHolder(v);
    }

    //sets values to view
    @Override
    public void onBindViewHolder(SongViewHolder personViewHolder, int i) {
        personViewHolder.songsName.setText(songs.get(i).getTitle());
        personViewHolder.songsArtist.setText(songs.get(i).getArtist());
        personViewHolder.songsArt.setImageBitmap(songs.get(i).getAlbumArt());
        personViewHolder.song = songs.get(i);
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    //item view holder
    public class SongViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView songsName;
        TextView songsArtist;
        ImageView songsArt;
        Song song;


        SongViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            songsName = (TextView)itemView.findViewById(R.id.songsName);
            songsArtist = (TextView)itemView.findViewById(R.id.songsArtist);
            songsArt = (ImageView)itemView.findViewById(R.id.songsArt);

            cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, songsName.getText(), Snackbar.LENGTH_SHORT).show();
                    JukeBox.play(song);
                }
            });

            cv.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    SongsDropdown popup = new SongsDropdown(MainView.getThis(), view, song);
                    popup.show();
                    return false;
                }
            });
        }

    }

}