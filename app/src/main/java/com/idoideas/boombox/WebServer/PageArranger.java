package com.idoideas.boombox.WebServer;

import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.Album;
import com.idoideas.boombox.Objects.Artist;
import com.idoideas.boombox.Objects.Playlist;
import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.Objects.SongsMachine;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Created by idoideas on 7/2/16.
 */
public class PageArranger {
    public enum Page{
        PAGE_MAIN_CSS,
        PAGE_DATA_SONGS_JS,
    }

    public static InputStream getPage(Page page){
            switch(page) {
                case PAGE_MAIN_CSS:
                    try {
                        return MainView.getContext().getAssets().open("CSS/page.css");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                case PAGE_DATA_SONGS_JS:
                    return pageDataSongs();
                default:
                    return pageDataSongs();
            }
        }

    public static InputStream pageDataSongs(){
        String listOfSongs = "listOfSongs = [";

        for (int i = 0; i<SongsMachine.getSongsNumber(); i++){
            listOfSongs+=createSongJS(SongsMachine.getSong(i))+",";
        }
        listOfSongs+="]; ";

        String listOfAlbums = "listOfAlbums = [";

        for (int i = 0; i<SongsMachine.getAlbumsNumber(); i++){
            listOfAlbums+=createAlbumJS(SongsMachine.getAlbum(i))+",";
        }
        listOfAlbums+="];";

        String listOfArtists = "listOfArtists = [";

        for (int i = 0; i<SongsMachine.getArtistsNumber(); i++){
            listOfArtists+=createArtistJS(SongsMachine.getArtist(i))+",";
        }
        listOfArtists+="];";

        String listOfPlaylists = "listOfPlaylists = [";

        for (int i = 0; i<SongsMachine.getPlaylistsNumber(); i++){
            listOfPlaylists+=createPlaylistJS(SongsMachine.getPlaylist(i))+",";
        }
        listOfPlaylists+="];";

        return StringToInputStream(listOfSongs+listOfAlbums+listOfArtists+listOfPlaylists);
    }

    private static String createSongJS(Song song){
        String songJS = "";
        songJS+="createSong(\""+song.getTitle();
        songJS+="\",\""+song.getArtist();
        songJS+="\",\""+song.hasAlbumArt();
        songJS+="\","+song.getId();
        if (song.hasAlbumArt()){
            songJS+=",\"/"+song.getId()+"?allow=1&image=1\"";
            songJS+=",\"/"+song.getId()+"?allow=1&image=1&icon=1\"";
        } else {
            songJS+=",\"images/album_not_found_placeholder.png\"";
            songJS+=",\"images/album_not_found_placeholder.png\"";
        }
        songJS+=")";
        return songJS;
    }
    private static String createAlbumJS(Album album){
        String albumJS = "";
        albumJS+="createAlbum(\""+album.getTitle();
        albumJS+="\",\""+album.getArtist();
        albumJS+="\",\""+album.hasAlbumArt();
        albumJS+="\","+album.getSong(0).getId();
        if (album.hasAlbumArt()){
            albumJS+=",\"/"+album.getSong(0).getId()+"?allow=1&image=1\"";
            albumJS+=",\"/"+album.getSong(0).getId()+"?allow=1&image=1&icon=1\"";
        } else {
            albumJS+=",\"images/album_not_found_placeholder.png\",";
        }
        albumJS+=",[";
        for (Song song : album.getAlbumSongs()) {
            albumJS += createSongJS(song)+",";
        }
        albumJS+="])";
        return albumJS;
    }

    //TODO: CHANGE IF I DECIDE TO SET IMAGE + IS THERE REALLY A CHANGE BETWEEN THIS AND ALBUM?
    private static String createArtistJS(Artist artist){
        String artistJS = "";
        artistJS+="createAlbum(\""+artist.getName();
        artistJS+="\",\"";
        artistJS+="\",\"";
        artistJS+="\","+0;
        artistJS+=",\"images/album_not_found_placeholder.png\"";
        artistJS+=",\"images/album_not_found_placeholder.png\"";
        artistJS+=",[";
        for (Song song : artist.getArtistSongs()) {
            artistJS += createSongJS(song)+",";
        }
        artistJS+="])";
        return artistJS;
    }

    private static String createPlaylistJS(Playlist playlist){
        String playlistJS = "";
        playlistJS+="createAlbum(\""+playlist.getTitle();
        playlistJS+="\",\"";
        playlistJS+="\",\"";
        playlistJS+="\","+0;
        playlistJS+=",\"images/album_not_found_placeholder.png\"";
        playlistJS+=",\"images/album_not_found_placeholder.png\"";
        playlistJS+=",[";
        for (Song song : playlist.getAllSongs()) {
            playlistJS += createSongJS(song)+",";
        }
        playlistJS+="])";
        return playlistJS;
    }
/*
    private static String getSongsMaterialView(ArrayList<Song> songs){
        String listOfSongs = "";
        for (int i = 0; i<songs.size(); i++){
            listOfSongs+="<a class='paper-item-link' onclick='listClick("+songs.get(i).getId()+"" +
                    ", \\'"+songs.get(i).getTitle().replace("'", "\\'")+"\\'" +
                    ", \\'"+songs.get(i).getArtist().replace("'", "\\'")+"\\'" +
                    ", \\'"+songs.get(i).hasAlbumArt()+"\\'" +
                    ");'>";
            listOfSongs+="<paper-icon-item>";
            if (SongsMachine.getSong(i).hasAlbumArt()){
                listOfSongs += "<img class='iconitem' src='/"+songs.get(i).getId()+"?allow=1&image=1' item-icon/>";
            } else {
                listOfSongs += "<img class='iconitem' src='images/album_not_found_placeholder.png' item-icon/>";
            }
            listOfSongs+="<paper-item-body two-line>";
            listOfSongs+="<div>"+songs.get(i).getTitle()+"</div>";
            listOfSongs+="<div secondary>"+songs.get(i).getArtist()+"</div>";
            listOfSongs+="</paper-item-body>";
            listOfSongs+="</paper-icon-item>";
            listOfSongs+="</a>";
        }
        return listOfSongs;
    }
    private static String getAlbumsMaterialView(ArrayList<Album> albums) {
        String listOfAlbums = "";
        for (int i = 0; i < albums.size(); i++) {
            listOfAlbums += "<a class=\\\"paper-item-link\\\" onclick=\\\"" +
                    "setSongsList(listOfSongs);\\\">";
            listOfAlbums += "<paper-icon-item>";
            if (SongsMachine.getSong(i).hasAlbumArt()) {
                listOfAlbums += "<img class=\\\"iconitem\\\" src=\\\"/" + albums.get(i).getSong(0).getId() + "?allow=1&image=1\\\" item-icon/>";
            } else {
                listOfAlbums += "<img class=\\\"iconitem\\\" src=\\\"images/album_not_found_placeholder.png\\\" item-icon/>";
            }
            listOfAlbums += "<paper-item-body two-line>";
            listOfAlbums += "<div>" + albums.get(i).getTitle() + "</div>";
            listOfAlbums += "<div secondary>" + albums.get(i).getArtist() + "</div>";
            listOfAlbums += "</paper-item-body>";
            listOfAlbums += "</paper-icon-item>";
            listOfAlbums += "</a>";
        }
        return listOfAlbums;
    }

    private static String InputStreamToString(InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    */
    private static InputStream StringToInputStream(String string){
        return new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_16));
    }
}
