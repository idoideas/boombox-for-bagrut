package com.idoideas.boombox.WebServer;

import android.os.Environment;
import android.util.Log;

import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.Objects.SongsMachine;
import com.idoideas.boombox.Resources.NanoHTTPD;
import com.idoideas.boombox.Resources.ServerRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by idoideas on 6/30/16.
 */
public class WebServer extends com.idoideas.boombox.Resources.NanoHTTPD {


    public WebServer(String hostname, int port) {
        super(hostname, port);
    }
    public WebServer(int port) {
        super(port);
    }

    @Override
    public Response serve(IHTTPSession session) {
        return ServeSession.Serve(session);
    }


    public static void main( String[] args )
    {

        ServerRunner.run(WebServer.class);
    }


}
