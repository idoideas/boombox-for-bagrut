package com.idoideas.boombox.WebServer;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.util.Log;

import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.Song;
import com.idoideas.boombox.Objects.SongsMachine;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by idoideas on 6/30/16.
 */
public class WebServerHandler {
    public enum ServerState{
        SERVER_NULL,
        SERVER_CREATED,
        SERVER_STARTED,
        SERVER_STOPPED
    }

    public static ServerState state = ServerState.SERVER_NULL;
    private static WebServer webServer;
    private static Thread webServerThread;
    private static int myPort;
    private static WifiManager wim= (WifiManager) MainView.getContext().getSystemService(Context.WIFI_SERVICE);

    //Creating web server in the background.
    public static void createWebServer(int port){
        Log.w("creating", "check!");
        myPort = port;
        webServer = new WebServer(port);
        webServerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    webServer.start();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                finally {
                    state = ServerState.SERVER_CREATED;
                }
            }
        });
    }

    //Starting the web server
    public static void startWebServer(){
        Log.w("Starting", "check!");
        state = ServerState.SERVER_STARTED;
        webServerThread.start();
    }

    //Stopping the web server
    public static void stopWebServer(){
        Log.w("Stopping", "check!");
        state = ServerState.SERVER_STOPPED;
        webServerThread.interrupt();
    }

    //Gets the server state (using enum)
    public static ServerState getState(){
        return state;
    }

    //Gets the local IP
    public static String getIP(){
        return Formatter.formatIpAddress(wim.getConnectionInfo().getIpAddress());
    }

    //Gets the server port
    public static int getPort(){
        if (webServer.getListeningPort()!=-1){
            myPort = webServer.getListeningPort();
        }
        return myPort;
    }

    //Gets the full server address
    public static String getAddress(){
        return getIP() + ":" + getPort();
    }

    //checks if server was created
    public static boolean isCreated(){
        return state!=ServerState.SERVER_NULL;
    }
}
