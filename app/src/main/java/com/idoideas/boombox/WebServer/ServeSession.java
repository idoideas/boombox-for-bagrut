package com.idoideas.boombox.WebServer;

import android.graphics.Bitmap;

import com.idoideas.boombox.MainView;
import com.idoideas.boombox.Objects.JukeBox;
import com.idoideas.boombox.Objects.SongsMachine;
import com.idoideas.boombox.Resources.NanoHTTPD;

import java.io.*;
import java.util.Map;

/**
 * Created by idoideas on 7/1/16.
 */
public class ServeSession {

    public enum RequestType{
        REQUEST_SONG,
        REQUEST_IMAGE
    }
    public enum ResponseType{
        RESPONSE_OK,
        RESPONSE_FORBIDDEN
    }

    public static NanoHTTPD.Response Serve(NanoHTTPD.IHTTPSession session){
        InputStream fis = null;
        String requestedDirectory = GetRequestedDirectory(session);

        //Checks if web file needed
        if(requestedDirectory.equals("")){
            try {
                return SendHTMLFile(MainView.getContext().getAssets().open("HTML/page.html"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(requestedDirectory.equals("API/data.js")){
            return SendJSFile(PageArranger.getPage(PageArranger.Page.PAGE_DATA_SONGS_JS));
        }
        else if(requestedDirectory.equals("addToPlaylist")){
            int p = Integer.parseInt(session.getParms().get("p"));
            int s = Integer.parseInt(session.getParms().get("s"));
            SongsMachine.getPlaylist(p).add(SongsMachine.getSong(s));
            return NotFound();
        }
        try {
            fis = MainView.getContext().getAssets().open(requestedDirectory);
        } catch (IOException e) {
            fis = null;
        }
        if (fis!=null){
            if(requestedDirectory.contains(".js")){
                return SendJSFile(fis);
            }
            else if (requestedDirectory.contains(".css")){
                return SendCSSFile(fis);
            }
            else if (requestedDirectory.contains(".html")){
                return SendHTMLFile(fis);
            }
            else if (requestedDirectory.contains(".ico") || requestedDirectory.contains(".jpeg")
                    || requestedDirectory.contains(".png")){
                return SendImageFile(fis);
            }
            else if (requestedDirectory.contains(".woff")){
                return SendFontFile(fis);
            }
        }

        //Sends media file
        RequestType requestType = checkRequestType(session);
        try {
            switch(requestType){
                case REQUEST_IMAGE:
                    fis = GetImageFile(GetParameterSongID(session), checkIconImage(session));
                    break;
                case REQUEST_SONG:
                    fis = GetMusicFile(GetParameterSongID(session));
                    break;
                default:
                    fis = GetMusicFile(GetParameterSongID(session));
                    break;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return NotFound();
        } catch (IndexOutOfBoundsException e){
            return NotFound();
        }

        if(checkAllow(session) == ResponseType.RESPONSE_FORBIDDEN){
            return Forbidden();
        }

        switch (requestType){
            case REQUEST_IMAGE:
                return SendImageFile(fis);
            case REQUEST_SONG:
                return SendMusicFile(fis, session);
            default:
                return SendMusicFile(fis, session);
        }
    }

    private static NanoHTTPD.Response SendMusicFile(InputStream fis, NanoHTTPD.IHTTPSession session) {
        File outputDir = MainView.getContext().getCacheDir();
        File outputFile = null;
        try {
            outputFile = File.createTempFile("tmpsng", "mp3", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        copyInputStreamToFile(fis, outputFile);
        return servePartialContent(outputFile,  session.getHeaders() ,"audio/mp3");
    }

    private static NanoHTTPD.Response SendImageFile(InputStream fis){
        return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "image/png", fis);
    }

    private static NanoHTTPD.Response SendHTMLFile(InputStream fis){
        return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "text/html", fis);
    }

    private static NanoHTTPD.Response SendCSSFile(InputStream fis){
        return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "text/css", fis);
    }

    private static NanoHTTPD.Response SendFontFile(InputStream fis){
        return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "application/x-font-woff", fis);
    }

    private static NanoHTTPD.Response SendJSFile(InputStream fis){
        NanoHTTPD.Response res = new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "application/javascript", fis);
        res.addHeader("Accept-Encoding","UTF-8");
        return res;
    }



    private static NanoHTTPD.Response NotFound(){
        try {
            return SendHTMLFile(MainView.getContext().getAssets().open("HTML/notFound.html"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new NanoHTTPD.Response("404 NOT FOUND");
    }

    private static NanoHTTPD.Response Forbidden(){
        try {
            return SendHTMLFile(MainView.getContext().getAssets().open("HTML/forbidden.html"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new NanoHTTPD.Response("403 FORBIDDEN");
    }

    private static FileInputStream GetMusicFile(int id){
        try {
            return new FileInputStream(SongsMachine.getSong(id).getPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static InputStream GetImageFile(int id, boolean isIcon){
        Bitmap base;
        if (isIcon){
            base = SongsMachine.getSong(id).getIconAlbumArt();
        }
        else{
            base = SongsMachine.getSong(id).getAlbumArt();
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        if (base != null) {
            base.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        }
        byte[] bitmapdata = bos.toByteArray();
        ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
        return bs;
    }

    private static int GetParameterSongID(NanoHTTPD.IHTTPSession session){
        return Integer.parseInt(GetRequestedDirectory(session));
    }

    private static String GetRequestedDirectory(NanoHTTPD.IHTTPSession session){
        String directory = session.getUri().substring(1);
        if(directory.contains("?"))
            directory = directory.split("\\?")[0];
        return directory;
    }
    private static RequestType checkRequestType(NanoHTTPD.IHTTPSession session){
        if (checkImage(session)){
            return RequestType.REQUEST_IMAGE;
        }
        return RequestType.REQUEST_SONG;
    }
    private static ResponseType checkAllow(NanoHTTPD.IHTTPSession session){
        if(session.getParms().containsKey("allow") && session.getParms().get("allow").equals("1"))
            return ResponseType.RESPONSE_OK;
        return ResponseType.RESPONSE_FORBIDDEN;
    }
    private static boolean checkImage(NanoHTTPD.IHTTPSession session){
        return session.getParms().containsKey("image") && session.getParms().get("image").equals("1");
    }
    private static boolean checkIconImage(NanoHTTPD.IHTTPSession session){
        return session.getParms().containsKey("icon") && session.getParms().get("icon").equals("1");
    }


    //FUNCTIONS FOR PARTIAL CONTENT SUPPORT
    private static NanoHTTPD.Response servePartialContent(File file, Map<String, String> header,
                                                          String mime) {
        NanoHTTPD.Response res;
        try {
            // Calculate etag
            String etag = Integer.toHexString((file.getAbsolutePath()
                    + file.lastModified() + "" + file.length()).hashCode());

            // Support (simple) skipping:
            long startFrom = 0;
            long endAt = -1;
            String range = header.get("range");
            if (range != null) {
                if (range.startsWith("bytes=")) {
                    range = range.substring("bytes=".length());
                    int minus = range.indexOf('-');
                    try {
                        if (minus > 0) {
                            startFrom = Long.parseLong(range
                                    .substring(0, minus));
                            endAt = Long.parseLong(range.substring(minus + 1));
                        }
                    } catch (NumberFormatException ignored) {
                    }
                }
            }

            // Change return code and add Content-Range header when skipping is
            // requested
            long fileLen = file.length();
            if (range != null && startFrom >= 0) {
                if (startFrom >= fileLen) {
                    res = createResponseWithRanges(NanoHTTPD.Response.Status.RANGE_NOT_SATISFIABLE,
                            NanoHTTPD.MIME_PLAINTEXT, null);
                    res.addHeader("Content-Range", "bytes 0-0/" + fileLen);
                    res.addHeader("ETag", etag);
                } else {
                    if (endAt < 0) {
                        endAt = fileLen - 1;
                    }
                    long newLen = endAt - startFrom + 1;
                    if (newLen < 0) {
                        newLen = 0;
                    }

                    final long dataLen = newLen;
                    FileInputStream fis = new FileInputStream(file) {
                        @Override
                        public int available() throws IOException {
                            return (int) dataLen;
                        }
                    };
                    fis.skip(startFrom);

                    res = createResponseWithRanges(NanoHTTPD.Response.Status.PARTIAL_CONTENT, mime,
                            fis);
                    res.addHeader("Content-Length", "" + dataLen);
                    res.addHeader("Content-Range", "bytes " + startFrom + "-"
                            + endAt + "/" + fileLen);
                    res.addHeader("ETag", etag);
                }
            } else {
                if (etag.equals(header.get("if-none-match")))
                    res = createResponseWithRanges(NanoHTTPD.Response.Status.NOT_MODIFIED, mime, null);
                else {
                    res = createResponseWithRanges(NanoHTTPD.Response.Status.OK, mime,
                            new FileInputStream(file));
                    res.addHeader("Content-Length", "" + fileLen);
                    res.addHeader("ETag", etag);
                }
            }
        } catch (IOException ioe) {
            res = createResponseWithRanges(NanoHTTPD.Response.Status.FORBIDDEN,
                    NanoHTTPD.MIME_PLAINTEXT, null);
        }

        return res;
    }

    private static NanoHTTPD.Response createResponseWithRanges(NanoHTTPD.Response.Status status, String mimeType,
                                                               InputStream message) {
        NanoHTTPD.Response res = new NanoHTTPD.Response(status, mimeType, message);
        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

    private static void copyInputStreamToFile(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
