function listClick(i){
    playSong(i);
}

function playSong(i){
    currentSong = i;
    document.getElementById('video').src ="/"+i+"?allow=1";
    if(listOfSongs[i].hasAlbumArt=="true"){
        document.getElementById('video').poster = "/"+i+"?allow=1&image=1";
    }
    else{
        document.getElementById('video').poster = "/images/album_not_found_placeholder.png";
    }
    if(listOfSongs[i].title.length>44){
        document.getElementById("songtitle").innerHTML = listOfSongs[i].title.substring(0,41)+"...";
    }
    else{
        document.getElementById("songtitle").innerHTML = listOfSongs[i].title;
    }
    document.getElementById("songartist").innerHTML = listOfSongs[i].artist;
    document.getElementById('video').play();
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    document.getElementById(data).parentElement.click();
}

function getQueueSongs(){
    newlist = [];
    if(currentSong!=undefined){
        newlist.push(listOfSongs[currentSong]);
    }
    for (i=0; i<queue.length; i++){
        newlist.push(listOfSongs[queue[i]]);
    }
    return newlist;
}

function closeMenu(object){
    object.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.opened = false;
}
