function createSong(title, artist, hasAlbumArt, id, albumArtPath, albumArtIconPath){
return {title: title, artist: artist, hasAlbumArt : hasAlbumArt, id: id, albumArtPath :albumArtPath, albumArtIconPath: albumArtIconPath};
}

function createAlbum(title, artist, hasAlbumArt, id, albumArtPath,  albumArtIconPath, songs){
return {title: title, artist: artist, hasAlbumArt : hasAlbumArt, id: id, albumArtPath :albumArtPath,  albumArtIconPath: albumArtIconPath, songs:songs};
}

function setSongsList(songs){
	currentlist = songs;
    songsMaterial="";
    for(i=0; i<songs.length; i++){
    songsMaterial+="<a class='paper-item-link song' ";
    songsMaterial+="onclick=\"listClick(";
    songsMaterial+=""+songs[i].id+"";
    songsMaterial+=");\"";
    songsMaterial+=">";
    songsMaterial+="<paper-icon-item ";
    songsMaterial+="id="+i+" draggable='true' ondragstart='drag(event)' ondragstart='drag(event)'>";
    if (songs[i].hasAlbumArt=="true"){
        image="/"+songs[i].id+"?allow=1&image=1";
        songsMaterial += "<img class='iconitem' src=\""+image+"\" item-icon/>";
    }else{
        songsMaterial += "<img class='iconitem' src='images/album_not_found_placeholder.png' item-icon/>";
    }
    songsMaterial += "<paper-item-body two-line>";
    songsMaterial+="<div>"+songs[i].title+"</div>";
    songsMaterial+="<div secondary>"+songs[i].artist+"</div>";
    songsMaterial+="</paper-item-body>";
    songsMaterial+="<paper-menu-button>";
    songsMaterial+="<paper-icon-button onclick='event.stopPropagation();' icon='arrow-drop-down' class='dropdown-trigger'></paper-icon-button>";
    songsMaterial+="<paper-menu onclick='event.stopPropagation();' class='dropdown-content'>";
    songsMaterial+="<paper-item onclick='setPlaylistsSelectionList(listOfPlaylists,"+songs[i].id+"); closeMenu(this);'>Add to playlist</paper-item>";
    songsMaterial+="<paper-item onclick='addToQueue("+songs[i].id+"); closeMenu(this);'>Add to queue</paper-item>";
    songsMaterial+="<paper-item onclick='playNext("+songs[i].id+"); closeMenu(this);'>Play next</paper-item>";
    songsMaterial+="</paper-menu>";
    songsMaterial+="</paper-menu-button>";
    songsMaterial+="</paper-icon-item>";
    songsMaterial+="</a>";
    }
    document.getElementsByClassName("list")[0].innerHTML=songsMaterial;
    document.getElementsByClassName("list")[0].scrollTop = 0;
    }

function setAlbumsList(albums){
	currentlist = albums;
    albumsMaterial="";
    for(i=0; i<albums.length; i++){
    albumsMaterial+="<a class='paper-item-link' onclick='";
    albumsMaterial+="setSongsList(listOfAlbums["+listOfAlbums.indexOf(albums[i])+"].songs)";
    albumsMaterial+="'>";
    albumsMaterial+="<paper-icon-item>";
    if (albums[i].hasAlbumArt=="true"){
        image="/"+albums[i].id+"?allow=1&image=1";
        albumsMaterial += "<img class='iconitem' src=\""+image+"\" item-icon/>";
    }else{
        albumsMaterial += "<img class='iconitem' src='images/album_not_found_placeholder.png' item-icon/>";
    }
    albumsMaterial+= "<paper-item-body two-line>";
    albumsMaterial+="<div>"+albums[i].title+"</div>";
    albumsMaterial+="<div secondary>"+albums[i].artist+"</div>";
    albumsMaterial+="</paper-item-body>";
    albumsMaterial+="</paper-icon-item>";
    albumsMaterial+="</a>";
    }
    document.getElementsByClassName("list")[0].innerHTML=albumsMaterial;
    document.getElementsByClassName("list")[0].scrollTop = 0
    }

    function setArtistsList(artists){
    	currentlist = artists;
        artistsMaterial="";
        for(i=0; i<artists.length; i++){
        artistsMaterial+="<a class='paper-item-link' onclick='";
        artistsMaterial+="setSongsList(listOfArtists["+listOfArtists.indexOf(artists[i])+"].songs)";
        artistsMaterial+="'>";
        artistsMaterial+="<paper-icon-item>";
        if (artists[i].hasAlbumArt=="true"){
            image="/"+artists[i].id+"?allow=1&image=1";
            artistsMaterial += "<img class='iconitem' src=\""+image+"\" item-icon/>";
        }else{
            artistsMaterial += "<img class='iconitem' src='images/album_not_found_placeholder.png' item-icon/>";
        }
        artistsMaterial+= "<paper-item-body two-line>";
        artistsMaterial+="<div>"+artists[i].title+"</div>";
        artistsMaterial+="<div secondary>"+artists[i].artist+"</div>";
        artistsMaterial+="</paper-item-body>";
        artistsMaterial+="</paper-icon-item>";
        artistsMaterial+="</a>";
        }
        document.getElementsByClassName("list")[0].innerHTML=artistsMaterial;
        document.getElementsByClassName("list")[0].scrollTop = 0
        }

function setPlaylistsList(playlists){
	currentlist = playlists;
    playlistsMaterial="";
    for(i=0; i<playlists.length; i++){
    playlistsMaterial+="<a class='paper-item-link' onclick='";
    playlistsMaterial+="reloadData(); setSongsList(listOfPlaylists["+listOfPlaylists.indexOf(playlists[i])+"].songs)";
    playlistsMaterial+="'>";
    playlistsMaterial+="<paper-icon-item>";
    if (playlists[i].hasAlbumArt=="true"){
        image="/"+playlists[i].id+"?allow=1&image=1";
        playlistsMaterial += "<img class='iconitem' src=\""+image+"\" item-icon/>";
    }else{
        playlistsMaterial += "<img class='iconitem' src='images/album_not_found_placeholder.png' item-icon/>";
    }
    playlistsMaterial+= "<paper-item-body two-line>";
    playlistsMaterial+="<div>"+playlists[i].title+"</div>";
    playlistsMaterial+="<div secondary>"+playlists[i].artist+"</div>";
    playlistsMaterial+="</paper-item-body>";
    playlistsMaterial+="</paper-icon-item>";
    playlistsMaterial+="</a>";
    }
    document.getElementsByClassName("list")[0].innerHTML=playlistsMaterial;
    document.getElementsByClassName("list")[0].scrollTop = 0
    }

function setPlaylistsSelectionList(playlists, songid){
	currentlist = playlists;
    playlistsMaterial="";
    for(i=0; i<playlists.length; i++){
    playlistsMaterial+="<a class='paper-item-link' onclick='";
    playlistsMaterial+="$.get(\"/addToPlaylist?p="+listOfPlaylists.indexOf(playlists[i]);
    playlistsMaterial+="&s="+songid+"\");";
    playlistsMaterial+="setSongsList(listOfPlaylists["+listOfPlaylists.indexOf(playlists[i])+"].songs)";
    playlistsMaterial+="'>";
    playlistsMaterial+="<paper-icon-item>";
    if (playlists[i].hasAlbumArt=="true"){
        image="/"+playlists[i].id+"?allow=1&image=1";
        playlistsMaterial += "<img class='iconitem' src=\""+image+"\" item-icon/>";
    }else{
        playlistsMaterial += "<img class='iconitem' src='images/album_not_found_placeholder.png' item-icon/>";
    }
    playlistsMaterial+= "<paper-item-body two-line>";
    playlistsMaterial+="<div>"+playlists[i].title+"</div>";
    playlistsMaterial+="<div secondary>"+playlists[i].artist+"</div>";
    playlistsMaterial+="</paper-item-body>";
    playlistsMaterial+="</paper-icon-item>";
    playlistsMaterial+="</a>";
    }
    document.getElementsByClassName("list")[0].innerHTML=playlistsMaterial;
    document.getElementsByClassName("list")[0].scrollTop = 0
    }


function setQueryList(text){
	if(text.length>1){
		newlist = [];
		for (i=0; i<listOfSongs.length; i++){
			if(listOfSongs[i].title.toLowerCase().includes(text.toLowerCase())){
				newlist.push(listOfSongs[i]);
			}
		}
		setSongsList(newlist);
    }
    else if(currentlist!=listOfSongs){
    	setSongsList(listOfSongs);
    }
}

function reloadData() {
        $('script[src="/API/data.js"]').remove();
        $('<script>').attr('src', "/API/data.js").attr("charset", "utf-8").appendTo('head');
    }

function setQueue(){
    queue = [];
    currentSong = undefined;
    $('video').on('ended',function(){
          if(queue[0]!=undefined){
                playSong(queue.shift(1));
          }
        });
}

function addToQueue(i){
    queue.push(i);
}

function playNext(i){
    queue.splice(0, 0, i);
}
